Getting Started
===============

Types of extensions
-------------------
There are four types of extensions:
  1. **Effect**: Takes the SVG from Inkscape, modifies the selection or the
     document and returns an SVG to Inkscape.
  2. **Generate**: Does not need any SVG, but instead just outputs an SVG
     fragment which is inserted into Inkscape, centered on the selection.
  3. **Input**: Takes any type of file as input and outputs SVG which Inkscape
     can read.
  4. **Output**: Takes the SVG from Inkscape and outputs it to something
     that’s not an SVG.

Extension Architecture
----------------------

Every extension needs two files: ``<name>.inx`` and ``<name>.py``. The names
should match up.

.. note::
   (Note: Using Python for the extensions is only a recommendation, but the
   documentation will focus exclusively on that method.)


Inx file
^^^^^^^^

:download:`Sample File </downloads/template.inx>`

The inx file is a specialized ``xml`` file. Its purpose is to tell Inkscape
various information about your extension. It follows the following format:

.. code-block:: xml

    <?xml version="1.0" encoding="UTF-8"?>
    <inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">

    <!-- 1. Replace this with the title of your extension. This will Show in the extensions menu-->
    <_name>Your Cool Title</_name>

    <!-- 2. Fill this in. It ensures that your extension has a unique ID -->
    <id>yourname.yourorganization.title</id>

    <!-- 3. Show the relative path to your extension. For example, `~/.config/inkscape/extensions/template.py` would be `template.py`-->
    <dependency type="executable" location="extensions">path/to/yourextension.py</dependency>
    <dependency type="executable" location="extensions">inkex.py</dependency>

    <effect>
        <object-type>all</object-type>
        <effects-menu>

        <!-- 4. Where is your extension located? (You can nest menus) -->
        <submenu _name="submenu"/>

        </effects-menu>
    </effect>
    <script>

        <!-- 5. Same as #4 -->
        <command reldir="extensions" interpreter="python">path/to/yourextension.py</command>

    </script>
    </inkscape-extension>

Python file
^^^^^^^^^^^

The ``python`` file is where most of the work will be done. The code will
always reference the inkex module in some way, so it may be worth your time
to take a look at it :ref:`here <inkex-extensions-module>`

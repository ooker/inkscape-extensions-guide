Hello World
===========

Example
-------

:download:`hello_word.py </downloads/hello_world.py>`

In this example, we will make a *GenerateExtension* which will render
the text "Hello World".

First, copy the ``inx`` template, rename it ``hello_world.inx``, and fill it in
with the title **Hello World**.

Next, create a file named ``hello_world.py``. We begin in our
``hello_world.py`` by importing the main inkex module, like so:

.. code-block:: python

  #!/usr/bin/env python
  # coding=utf-8

  import inkex

  from inkex.elements import TextElement

You will notice that we also imported ``TextElement`` from the *elements*
submodule. This allows us to create a text node. We now create the class
for our extension by extending the ``inkex.GenerateExtension`` class.

.. code-block:: python

  class Greet(inkex.GenerateExtension):
    def generate(self):

In the documentation there are many methods to use. We will just use the
``generate`` method, because it serves our purpose. It simply requires us to
return an SVG node, which will then be inserted into the document.

Now, we can write the logic. We first create the text node, then set the
``text`` attribute to "Hello World", and finally return the text node.

.. code-block:: python

    textElement = TextElement()
    textElement.text = 'Hello World'
    return textElement

Next Steps
----------

Try these things on your own.
 - Make the text a different color, size, or font.
 - Place the text on a different position on the canvas.
 - Currently, long text will render on one line. Try to accomodate for long
   strings using flowed text.

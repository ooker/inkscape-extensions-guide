Getting Help
============

If you're stuck, try these couple of resources.
 - **Forums**: https://inkscape.org/forums/
 - **Rocket.chat**: https://chat.inkscape.org/channel/inkscape_extensions

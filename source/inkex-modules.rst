Inkex Submodules
================

inkex.base module
-----------------

.. automodule:: inkex.base
    :members:
    :undoc-members:
    :show-inheritance:

inkex.bezier module
-------------------

.. automodule:: inkex.bezier
    :members:
    :undoc-members:
    :show-inheritance:

inkex.colors module
-------------------

.. automodule:: inkex.colors
    :members:
    :undoc-members:
    :show-inheritance:

inkex.command module
--------------------

.. automodule:: inkex.command
    :members:
    :undoc-members:
    :show-inheritance:

inkex.deprecated module
-----------------------

.. automodule:: inkex.deprecated
    :members:
    :undoc-members:
    :show-inheritance:

inkex.elements module
---------------------

.. automodule:: inkex.elements
    :members:
    :undoc-members:
    :show-inheritance:


.. _inkex-extensions-module:

inkex.extensions module
-----------------------

.. automodule:: inkex.extensions
    :members:
    :undoc-members:
    :show-inheritance:

inkex.localization module
-------------------------

.. automodule:: inkex.localization
    :members:
    :undoc-members:
    :show-inheritance:

inkex.paths module
------------------

.. automodule:: inkex.paths
    :members:
    :undoc-members:
    :show-inheritance:

inkex.styles module
-------------------

.. automodule:: inkex.styles
    :members:
    :undoc-members:
    :show-inheritance:

inkex.svg module
----------------

.. automodule:: inkex.svg
    :members:
    :undoc-members:
    :show-inheritance:

inkex.transforms module
-----------------------

.. automodule:: inkex.transforms
    :members:
    :undoc-members:
    :show-inheritance:

inkex.turtle module
-------------------

.. automodule:: inkex.turtle
    :members:
    :undoc-members:
    :show-inheritance:

inkex.tween module
------------------

.. automodule:: inkex.tween
    :members:
    :undoc-members:
    :show-inheritance:

inkex.units module
------------------

.. automodule:: inkex.units
    :members:
    :undoc-members:
    :show-inheritance:

inkex.utils module
------------------

.. automodule:: inkex.utils
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: inkex
    :members:
    :undoc-members:
    :show-inheritance:
